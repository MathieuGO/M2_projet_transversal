---
title: "Génération des tableaux de fréquences"
author: "Nicolas"
date: "19 décembre 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library('readr') # lire les csv (avec définition des types de champs et de l'encodage du fichier)
library("reshape2") # pour la fonction dcast
library('questionr') # pour le tableau pondéré (fonction wtd.table())
library('dplyr') # manipuler les données (filter(), select(),..)
library('purrr')
```
# Chargement des données 2015


```{r}
VIVIENDA_PERSONA_15 <- read_csv("../data/traitees/persona_vivienda_zm_mexico_2015_concentration.csv", 
    col_types = cols(ID_PERSONA = col_character(), 
        ID_VIV = col_character()), locale = locale())
```
```{r variables globales}
# récupaération des ID des Municpes
tab_freq_stocks <- unique(VIVIENDA_PERSONA_15['CODE_MUN']) # pour les stocks
colnames(tab_freq_stocks) <- c('code_mun')
tab_freq_pourc <- tab_freq_stocks # pour les pourcentages
```
# Exemple sur une variable
### CLAVIVP ###


```{r}
VIVIENDA_PERSONA_CLAVIVP <- as.data.frame(wtd.table(VIVIENDA_PERSONA_15$CODE_MUN,VIVIENDA_PERSONA_15$CLAVIVP, weights = VIVIENDA_PERSONA_15$FACTOR_VIV )) 

TabCLAVIVP <- as.data.frame(dcast(VIVIENDA_PERSONA_CLAVIVP,formula=VIVIENDA_PERSONA_CLAVIVP$Var1~VIVIENDA_PERSONA_CLAVIVP$Var2))
colnames(TabCLAVIVP) <- c("code_mun", "CLAVIVP_1",  "CLAVIVP_2",  "CLAVIVP_3",  "CLAVIVP_4",  "CLAVIVP_5",  "CLAVIVP_6",  "CLAVIVP_7",  "CLAVIVP_8",  "CLAVIVP_9", "CLAVIVP_99")
```

# Variables Vivienda

```{r tableaux de frequence vivienda}
# Tableau contenant les noms des variables sélectionnées
#variables <- c('CLAVIVP') # exemple
variables_choisies <- read_csv("../data/variables_choisies_vivienda.txt")
variables <- variables_choisies[10:37,]$variable
#print(variables)

# tableau de fréquence vide (ou presque)
tab_freq_temp <- character()

# récupération des code mun
tab_freq <- tab_freq_stocks

## Création tableau vide pour les fréquences en pourcentage (ACP)
tab_freq_pourc <- tab_freq

#tab_freq <- merge(tab_freq, VIVIENDA_PERSONA_15['CODE_MUN','ENT','NOM_ENT','NOM_MUN','FACTOR_VIV'], by.x="code_mun", by.y = "CODE_MUN", all = "TRUE")

# chargement des code_mun et des factors pour utilisation dans la boucle
code   <- VIVIENDA_PERSONA_15$CODE_MUN
factor <- VIVIENDA_PERSONA_15$FACTOR_VIV 
nouveaux_noms_cols <- character()

for (variable  in variables){
  # création du nom de variable
  nom_variable <- paste('VIV_',variable, sep='')
  # récupération des valeurs en fonction du nom de variable
  temp <- t(select(VIVIENDA_PERSONA_15, variable))
  
  ## debuggage
  print(paste("Working on", nom_variable))

  
  # création d'une table temporaire stockant les variables pondérées
  tab_freq_temp <- assign(nom_variable, as.data.frame(wtd.table(code,temp, weights = factor)))
  
  
  ## dcast !! => extraction des modalités de la colonnes Var1
  temp2 <- as.data.frame(dcast(tab_freq_temp,formula=tab_freq_temp$Var1~tab_freq_temp$Var2))
  
  ### renommage des colonnes
  i = 0
  for(name in colnames(temp2)){
    if (i == 0){ nouveaux_noms_cols <- c('code_mun')}
    else{nouveaux_noms_cols <- c(nouveaux_noms_cols, paste(variable, '_', name, sep =''))}
    i = i + 1
    #print(nouveaux_noms_cols)
  }
  
  # affectation des nouveaux noms
  colnames(temp2) <- nouveaux_noms_cols

  # vérification que la somme fait bien 100%
  #total_perc <- as.data.frame(apply(pcts, 1, sum))
  
  ### fusion à la table finale
  tab_freq <- merge(tab_freq, temp2, BY="code_mun", all = "TRUE")
  
  # somme des lignes
  total_col = apply(temp2[,-1], 1, sum)
  
  # Calcul des pourcentages par lignes
  pourcentages <- cbind(tab_freq['code_mun'], as.data.frame(lapply(temp2[,-1], function(x) {
  x / total_col})))
  
  # stockage dans la variable globale
  tab_freq_pourc <- merge(tab_freq_pourc, pourcentages, BY="code_mun", all = "TRUE")
  
  ## nettoyage mémoire
  rm(temp,temp2,tab_freq_temp, nouveaux_noms_cols, nom_variable,i,name, variable, total_col)
}

# nettoyage des variables
rm(code, factor, variables)
dim(tab_freq)
rm(list=ls(pattern="VIV_"))

```

```{r sauvegarde du tableau de fréquence viv dans un csv}
write.csv(tab_freq, file = "../data/traitees/tab_frequence_2015_viv_stocks.csv",row.names=FALSE)# export en csv
write.csv(tab_freq_pourc, file = "../data/traitees/tab_frequence_2015_viv_perc.csv",row.names=FALSE)# export en csv
## stockage dans un fichier complet
tab_freq_stocks <- merge(tab_freq_stocks, tab_freq, BY="code_mun", all = "TRUE")
```

```{r}
dim(TabCLAVIVP)
dim(tab_freq)
all.equal(TabCLAVIVP,tab_freq)
identical(TabCLAVIVP,tab_freq)
#colnames(TabCLAVIVP)
```

```{r nettoyage partiel}
rm(TabCLAVIVP,tab_freq,pourcentages)
```

# Variables persona

```{r}
# Tableau contenant les noms des variables sélectionnées
#variables <- c('CLAVIVP') # exemple
variables_choisies <- read_csv("../data/variables_choisies_persona.txt")
variables <- variables_choisies[10:15,]$variable
#print(variables)

# tableau de fréquence vide (ou presque)
tab_freq_temp <- character()

# récupération des code mun
tab_freq <- unique(VIVIENDA_PERSONA_15[,'CODE_MUN'])

#renommage des code mun
colnames(tab_freq) <- c('code_mun')
#tab_freq <- merge(tab_freq, VIVIENDA_PERSONA_15['CODE_MUN','ENT','NOM_ENT','NOM_MUN','FACTOR_VIV'], by.x="code_mun", by.y = "CODE_MUN", all = "TRUE")

# chargement des code_mun et des factors pour utilisation dans la boucle
code   <- VIVIENDA_PERSONA_15$CODE_MUN
factor <- VIVIENDA_PERSONA_15$FACTOR_PERS 
nouveaux_noms_cols <- character()
for (variable  in variables){
  # création du nom de variable
  nom_variable <- paste('PERS_',variable, sep='')
  
  # récupération des valeurs en fonction du nom de variable
  temp <- t(select(VIVIENDA_PERSONA_15, variable))
  
  ## debuggage
  print(paste("Working on", nom_variable))
  
  # création d'une table temporaire stockant les variables pondérées
  tab_freq_temp <- assign(nom_variable, as.data.frame(wtd.table(code,temp, weights = factor)))
  
  ## dcast !!
  #print("dcast")
  temp2 <- as.data.frame(dcast(tab_freq_temp,formula=tab_freq_temp$Var1~tab_freq_temp$Var2))
  
  
  ### renommage des colonnes
  i = 0
  for(name in colnames(temp2)){
    if (i == 0){ nouveaux_noms_cols <- c('code_mun')}
    else{nouveaux_noms_cols <- c(nouveaux_noms_cols, paste(variable, '_', name, sep =''))}
    i = i + 1
    #print(nouveaux_noms_cols)
  }
  
  # Affectation des nouveaux noms de colonnes
  colnames(temp2) <- nouveaux_noms_cols
  
  ### fusion à la table finale
  tab_freq <- merge(tab_freq, temp2, BY="code_mun", all = "TRUE")
  
  
  # Création de la table de fréquence en pourcentage
  total_col = apply(temp2[,-1], 1, sum)

  pourcentages <- cbind(tab_freq['code_mun'], as.data.frame(lapply(temp2[,-1], function(x) {
  x / total_col})))
  
  # stockage dans la variable globale
  tab_freq_pourc <- merge(tab_freq_pourc, pourcentages, BY="code_mun", all = "TRUE") 
  
  ## nettoyage mémoire
  rm(temp,tab_freq_temp, temp2, nouveaux_noms_cols, nom_variable,i,name, variable)
}

# nettoyage des variables
rm(code, factor, variables)
rm(list=ls(pattern="PERS_")) #remove objects from a a name list
dim(tab_freq)
```
```{r sauvegarde du tableau de fréquence pers dans un csv}
write.csv(tab_freq, file = "../data/traitees/tab_frequence_2015_pers_stocks.csv",row.names=FALSE)# export en csv
write.csv(pourcentages, file = "../data/traitees/tab_frequence_2015_pers_perc.csv",row.names=FALSE)# export en csv

## stockage dans un fichier complet
tab_freq_stocks <- merge(tab_freq_stocks, tab_freq, BY="code_mun", all = "TRUE")
tab_freq_pourc <- merge(tab_freq_pourc, pourcentages, BY="code_mun", all = "TRUE")

saveRDS(tab_freq_stocks,"../data/traitees/tab_freq_stocks_pers_viv.rds", compress = TRUE)
saveRDS(tab_freq_pourc,"../data/traitees/tab_freq_pourc_pers_viv.rds", compress = TRUE)
```

```{r nettoyage final}
rm(pourcentages, variables_choisies, VIVIENDA_PERSONA_15, VIVIENDA_PERSONA_CLAVIVP,tab_freq_stocks,tab_freq_pourc)
rm(list=ls(pattern="tab_"))
```

