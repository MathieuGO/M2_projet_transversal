---
title: "MexicoDataFreq"
output: html_document
author: "auteur: Mathieu Gheno, relecture: Nicolas Roelandt"
---

# Test sur une seule entité
## Chargement des donnees sur les foyers mexicains
```{r read.csv}
#VIVIENDA15 <- read.csv("TR_VIVIENDA15.csv",header=TRUE, sep=",")

library(readr)
VIVIENDA15 <- read_csv("data/CSV/TR_VIVIENDA15.CSV", 
    col_types = cols(ENT = col_character(), 
        ID_VIV = col_character(), NOM_ENT = col_character()), 
    locale = locale(encoding = "ISO-8859-1"))

```

# Conversion des variables d'identification en chaine de caractère

Les variables ID_IV, ENT, MUN sont les variables d'identification des entités géographiques. Suite à l'importation du fichier TR_VIVIENDA15.csv sous R, elles appraissent comme type "facteur". Ces variables sont transformées en type caractère par mesure de précaution pour permettre la concaténation de ces variables.

```{r as.character}
#VIVIENDA15$ID_VIV <- as.character(VIVIENDA15$ID_VIV)
#VIVIENDA15$ENT <- as.character(VIVIENDA15$ENT)
#VIVIENDA15$MUN <- as.character(VIVIENDA15$MUN)
```

La fonction *read_csv* appelée précédemment permet de contourner cette étape dès le chargement du csv.

# Création d'un identifiant code municipale (code_mun) par concaténation du code ENT et MUN

Un identifiant géographique qui reprend les codes des entités géographiques supérieurs est créé et servira de maille géographique de travail pour comparer les individus ou entités géographiques au niveau municipale.

```{r cbind}
VIVIENDA15$code_mun <- cbind(paste(VIVIENDA15$ENT,VIVIENDA15$MUN,sep=""))
#attributes(VIVIENDA15)
```

# Utilisation de bibliothèque questionr 

Les valeurs présentes dans la table TR_VIVIENDA15 sont qualitatives. Pour effectuer des traitements statistiques, il faut traduire cette information en valeur quantitative. Pour cela, il faut compter la fréquence d'apparition des valeurs quanlitatives pour chaque entité géographique. Le tri croisé avec pondération permet de prendre en compte le fait qu'il s'agit d'un recensement par échantillonnage et de transposer les valeurs vers un population plus grande (typiquement à la population totale du Mexique).

La bibliothèque *questionr* est utile pour transformer les modalités prises par les variables en fréquences.

```{r library}
library(questionr)
```

## Tri croisé avec pondération

La fonction *wtd.table()* du package *questionr* permet dans cet exemple de compter le nombre d'occurence prise par les modalités (*Si*, *No*, *No especificado*) pour la variable REFRIGERADOR pour chaque *Municipio o Delegaciòn*, le tout pondéré par la variable Factor. La variable Factor marque la pondération des individus préconisée par l'INEGI.

Exemple avec la variable *Bienes y TIC (refrigerador)* qui porte le numéro 45 dans la description des variables fournie par l'INEGI. 

```{r wtd.table}
VIVIENDA_45 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$REFRIGERADOR, weights = VIVIENDA15$FACTOR )) 
head(VIVIENDA_45)
```

Var1: code MUN

Var2: modalités, ici 1, 2 ou 9; triées dans l'ordre croissant

Freq: fréquence de la modalité dans le Municipe.

## Découpage du tableau issu du tri croisé en plusieurs objets

Le problème qui survient suite au tri croisé, est la création d'un tableau dans lesquels les modalités qualitatives ne sont toujours pas les variables et dans lesquels ces modalités sont présentes dans la mếme colonne. Il faut donc extraire chaque ligne qui correspond à la même modalité pour créer en quelque sorte une colonne par modalité.

```{r []}
#Code_Mun <- VIVIENDA_45[,1]
colnames(VIVIENDA_45) <- c("code_mun", "modalite", "frequence")


# il vaut mieux filter par valeur dplyr, fonction filter() => éviter les problème d'ordre dans les individus
#REFRIGERADOR_SI <- VIVIENDA_45[1:125,3]
#REFRIGERADOR_NO <- VIVIENDA_45[126:250,3]
#REFRIGERADOR_9<- VIVIENDA_45[251:375,3]

library(dplyr)

REFRIGERADOR_SI <- filter(VIVIENDA_45, modalite == 1) 
REFRIGERADOR_NO <- filter(VIVIENDA_45, modalite == 2)
REFRIGERADOR_9  <- filter(VIVIENDA_45, modalite == 9)


## supression de la colonne modalité
REFRIGERADOR_SI <- select(REFRIGERADOR_SI, c('code_mun','frequence'))
REFRIGERADOR_NO <- select(REFRIGERADOR_NO, c('code_mun','frequence'))
REFRIGERADOR_9  <- select(REFRIGERADOR_9, c('code_mun','frequence'))

## Renommage des colonnes
names(REFRIGERADOR_SI)[names(REFRIGERADOR_SI) == 'frequence'] <- 'refrigerador_si'
names(REFRIGERADOR_NO)[names(REFRIGERADOR_NO) == 'frequence'] <- 'refrigerador_no'
names(REFRIGERADOR_9)[names(REFRIGERADOR_9) == 'frequence'] <- 'refrigerador_9'


```

## Création d'un tableau avec les différents objets découpés

Une fois les modalités extraites, il faut créer un tableau en assemblant chaque colonne prise par chaque modalité. => ** tab_freq_equipement** ? en cumulant directement par *code_mun* ?

Un autre problème survient suite à cette opération qui est le dédoublement des lignes. Le tableau contiendra TabRefregirador contient 3 fois la même lignes pour chaque individu. => **group by** *ID_VIDENDA*? avec une fonction *somme* ou *max* ?

```{r data.frame}
TabRefregirador <- merge(REFRIGERADOR_SI, REFRIGERADOR_NO, BY="code_mun", all = "TRUE")
TabRefregirador <- merge(TabRefregirador, REFRIGERADOR_9, BY="code_mun", all = "TRUE")
```

# Application à une série de données plus grande: toutes les données et toutes les variables sélectionnées

##  Chargements des 3 Entités fédératives de la zone d'étude
```{r chargement des librairies et fichiers}
library('readr')
library('dplyr')
# Chargement des donnees sur les foyers mexicains
VIVIENDA09 <- read_csv("data/CSV/TR_VIVIENDA09.CSV", 
    col_types = cols(ENT = col_character(), 
        ID_VIV = col_character(), LOC50K = col_character(), 
        MUN = col_character(), NOM_ENT = col_character()))

VIVIENDA13 <- read_csv("data/CSV/TR_VIVIENDA13.CSV", 
    col_types = cols(ENT = col_character(), 
        ID_VIV = col_character(), LOC50K = col_character(), 
        MUN = col_character(), NOM_ENT = col_character()))

VIVIENDA15 <- read_csv("data/CSV/TR_VIVIENDA15.CSV", 
    col_types = cols(ENT = col_character(), 
        ID_VIV = col_character(), UPM = col_character()), 
    locale = locale(encoding = "ISO-8859-1"))
```

## Fusion  des bases

```{r fusion des fichiers VIVIENDA}
### formation d'un dataframe unique
tous_vivienda <- bind_rows(VIVIENDA09,VIVIENDA13,VIVIENDA15)

nbre_viv_brut <- dim(VIVIENDA09)[1] + dim(VIVIENDA13)[1] + dim(VIVIENDA15)[1]
print(paste("Nombre d'individus dans les 3 entités fédératives:", nbre_viv_brut))
print(paste("Nombre d'individus dans la base de regroupement:", dim(tous_vivienda)[1]))

if(nbre_viv_brut == dim(tous_vivienda)[1]){
  print("Le nombre d'enregistrements est identique.")} else {
  print("Le nombre d'enregistrements n'est pas identique.") 
  }

# nettoyage de la mémoire
rm(nbre_viv_brut,VIVIENDA09,VIVIENDA13,VIVIENDA15)
```
## Filtrage des codes MUN
```{r filtrage par code MUN}
## Ajout de la colonne code_mun
tous_vivienda$code_mun <- cbind(paste(tous_vivienda$ENT,tous_vivienda$MUN,sep=""))
dim(tous_vivienda)
### Filtrage des fichiers sur le code MUN
liste_municipalites <- read_csv("data/Municipalites_ZM_1.csv")


tous_vivienda_filtre <- filter(tous_vivienda, code_mun %in% liste_municipalites$C)


nb_mun_zum <- dim(liste_municipalites)[1]
nb_mun_base_unique <- dim(unique(tous_vivienda_filtre$code_mun))[1]

print(paste("Nombre de Municipe dans les 3 entités fédératives:",nb_mun_zum ))
print(paste("Nombre dans la base de regroupement:",nb_mun_base_unique ))

if(nb_mun_zum == nb_mun_base_unique){
  print("Le nombre de Municipe est identique.")
  print(paste("Nombre d'enregistrements conservés: ", dim(tous_vivienda_filtre)[1]))
  } else {
  print("Le nombre de Municipe n'est pas identique.") 
  }


# Nettoyage de la mémoire
rm(nb_mun_zum,nb_mun_base_unique,tous_vivienda)
```
## Filtrage des variables
```{r filtrage des variables}
variables_choisies <- read_csv("data/variables_choisies_vivienda.txt")
variables_choisies[order(variables_choisies$variable), ]
print(paste("Nombre de variables sélectionnées: ",dim(variables_choisies)[1]))


vivienda_reduit <-tous_vivienda_filtre[,variables_choisies$variable]
print(paste("La base réduite contient ",dim(vivienda_reduit)[1],"enregistrements et ",dim(vivienda_reduit)[2]," variables"))
```

## Export en CSV
```{r export_csv_base_reduite}
write.csv(vivienda_reduit, file = "data/traitees/vivienda_reduit.csv",row.names=FALSE, na="")
```
```{r nettoyage mémoire}
rm(liste_municipalites,tous_vivienda_filtre,vivienda_reduit,variables_choisies)
```

# Pondération des variables

```{r library}
library(questionr)
library(dplyr)
```

```{r}
# test si l'objet vivienda_reduit existe sinon charge la base depuis le csv
if(!exists("vivienda_reduit")){
  print("L'objet vivienda_reduit n'existe pas. Chargement depuis le csv...")
  library('readr')
  vivienda_reduit <- read_csv("data/traitees/vivienda_reduit.csv", 
    col_types = cols(ENT = col_character(), 
        ID_VIV = col_character()), locale = locale(encoding = "ISO-8859-1"))
  print(paste("La base chargée contient ",dim(vivienda_reduit)[1],"enregistrements et ",dim(vivienda_reduit)[2]," variables"))
}else{
  print("L'objet vivienda_reduit existe déjà.")
}
```

## Création d'un base unique réceptacle de toutes les variables pondérées et aggrégées à la municipalité
```{r}
tab_freq <- unique(vivienda_reduit$code_mun)
```

```{r VIVIENDA_45}
VIVIENDA_45 <- as.data.frame(wtd.table(vivienda_reduit$code_mun,vivienda_reduit$REFRIGERADOR, weights = vivienda_reduit$FACTOR )) 
#head(VIVIENDA_45)
colnames(VIVIENDA_45) <- c("code_mun", "modalite", "frequence")

# filtrage des modalités
REFRIGERADOR_SI <- filter(VIVIENDA_45, modalite == 1) 
REFRIGERADOR_NO <- filter(VIVIENDA_45, modalite == 2)
REFRIGERADOR_9  <- filter(VIVIENDA_45, modalite == 9)


## supression de la colonne modalité
REFRIGERADOR_SI <- select(REFRIGERADOR_SI, c('code_mun','frequence'))
REFRIGERADOR_NO <- select(REFRIGERADOR_NO, c('code_mun','frequence'))
REFRIGERADOR_9  <- select(REFRIGERADOR_9, c('code_mun','frequence'))

## Renommage des colonnes
names(REFRIGERADOR_SI)[names(REFRIGERADOR_SI) == 'frequence'] <- 'refrigerador_si'
names(REFRIGERADOR_NO)[names(REFRIGERADOR_NO) == 'frequence'] <- 'refrigerador_no'
names(REFRIGERADOR_9)[names(REFRIGERADOR_9) == 'frequence'] <- 'refrigerador_9'

## incorporation dans tab_freq
tab_freq <- merge(tab_freq, REFRIGERADOR_SI, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, REFRIGERADOR_NO, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, REFRIGERADOR_9, BY="code_mun", all = "TRUE")

## effacement de la colonne x créé inutilement
tab_freq$x <- NULL

# Nettoyage de la mémoire
rm(REFRIGERADOR_SI,REFRIGERADOR_NO,REFRIGERADOR_9,VIVIENDA_45)
```

```{r VIVIENDA_46}
# #Cr?ation tableau de contingence pour la variable LAVADORA
VIVIENDA_46 <- as.data.frame(wtd.table(vivienda_reduit$code_mun,vivienda_reduit$LAVADORA, weights = vivienda_reduit$FACTOR))
# Code_Mun <- VIVIENDA_46[,1]
# LAVADORA_SI <- VIVIENDA_46[1:125,3]
# LAVADORA_NO <- VIVIENDA_46[126:250,3]
# LAVADORA_9<- VIVIENDA_46[251:375,3]
# TabLavadora <- data.frame(Code_Mun,LAVADORA_SI,LAVADORA_NO,LAVADORA_9)

colnames(VIVIENDA_46) <- c("code_mun", "modalite", "frequence")

# filtrage des modalités
modalite_si <- filter(VIVIENDA_46, modalite == 3) 
modalite_no <- filter(VIVIENDA_46, modalite == 4)
modalite_9  <- filter(VIVIENDA_46, modalite == 9)


## supression de la colonne modalité
modalite_si <- select(modalite_si, c('code_mun','frequence'))
modalite_no <- select(modalite_no, c('code_mun','frequence'))
modalite_9  <- select(modalite_9, c('code_mun','frequence'))

## Renommage des colonnes
names(modalite_si)[names(modalite_si) == 'frequence'] <- 'lavadora_si'
names(modalite_no)[names(modalite_no) == 'frequence'] <- 'lavadora_no'
names(modalite_9)[names(modalite_9) == 'frequence'] <- 'lavadora_9'

## incorporation dans tab_freq
tab_freq <- merge(tab_freq, modalite_si, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, modalite_no, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, modalite_9, BY="code_mun", all = "TRUE")

## effacement de la colonne x créé inutilement
tab_freq$x <- NULL

# Nettoyage de la mémoire
rm(modalite_si,modalite_no,modalite_9,VIVIENDA_46)
```

```{r VIVIENDA_47}
#Création tableau de contingence pour la variable 47 HORNO
VIVIENDA_47 <- as.data.frame(wtd.table(vivienda_reduit$code_mun,vivienda_reduit$HORNO, weights = vivienda_reduit$FACTOR))
# Code_Mun <- VIVIENDA_47[,1]
# HORNO_SI <- VIVIENDA_47[1:125,3]
# HORNO_NO <- VIVIENDA_47[126:250,3]
# HORNO_9<- VIVIENDA_47[251:375,3]
# 
# TabHorno <- data.frame(Code_Mun,HORNO_SI,HORNO_NO,HORNO_9)

colnames(VIVIENDA_47) <- c("code_mun", "modalite", "frequence")

# filtrage des modalités
modalite_si <- filter(VIVIENDA_47, modalite == 5) 
modalite_no <- filter(VIVIENDA_47, modalite == 6)
modalite_9  <- filter(VIVIENDA_47, modalite == 9)


## supression de la colonne modalité
modalite_si <- select(modalite_si, c('code_mun','frequence'))
modalite_no <- select(modalite_no, c('code_mun','frequence'))
modalite_9  <- select(modalite_9, c('code_mun','frequence'))

## Renommage des colonnes
names(modalite_si)[names(modalite_si) == 'frequence'] <- 'horno_si'
names(modalite_no)[names(modalite_no) == 'frequence'] <- 'horno_no'
names(modalite_9)[names(modalite_9) == 'frequence'] <- 'horno_9'

## incorporation dans tab_freq
tab_freq <- merge(tab_freq, modalite_si, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, modalite_no, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, modalite_9, BY="code_mun", all = "TRUE")

## effacement de la colonne x créé inutilement
tab_freq$x <- NULL

# Nettoyage de la mémoire
rm(modalite_si,modalite_no,modalite_9,VIVIENDA_47)
```

```{r VIVIENDA_48}
#Cr?ation tableau de contingence pour la variable AUTOPROP
VIVIENDA_48 <- as.data.frame(wtd.table(vivienda_reduit$code_mun,vivienda_reduit$AUTOPROP, weights = vivienda_reduit$FACTOR))
# Code_Mun <- VIVIENDA_48[,1]
# AUTOPROP_SI <- VIVIENDA_48[1:125,3]
# AUTOPROP_NO <- VIVIENDA_48[126:250,3]
# AUTOPROP_9<- VIVIENDA_48[251:375,3]

# TabAutodrop<- data.frame(Code_Mun,AUTOPROP_SI,AUTOPROP_NO,AUTOPROP_9)

colnames(VIVIENDA_48) <- c("code_mun", "modalite", "frequence")

# filtrage des modalités
modalite_si <- filter(VIVIENDA_48, modalite == 7) 
modalite_no <- filter(VIVIENDA_48, modalite == 8)
modalite_9  <- filter(VIVIENDA_48, modalite == 9)


## supression de la colonne modalité
modalite_si <- select(modalite_si, c('code_mun','frequence'))
modalite_no <- select(modalite_no, c('code_mun','frequence'))
modalite_9  <- select(modalite_9, c('code_mun','frequence'))

## Renommage des colonnes
names(modalite_si)[names(modalite_si) == 'frequence'] <- 'horno_si'
names(modalite_no)[names(modalite_no) == 'frequence'] <- 'horno_no'
names(modalite_9)[names(modalite_9) == 'frequence'] <- 'horno_9'

## incorporation dans tab_freq
tab_freq <- merge(tab_freq, modalite_si, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, modalite_no, BY="code_mun", all = "TRUE")
tab_freq <- merge(tab_freq, modalite_9, BY="code_mun", all = "TRUE")

## effacement de la colonne x créé inutilement
tab_freq$x <- NULL

# Nettoyage de la mémoire
rm(modalite_si,modalite_no,modalite_9,VIVIENDA_48)
```
```{r sauvegarde du tableau de fréquence dans un csv}
#write.csv(tab_freq, file = "data/traitees/2015_VIVIENDA_EQUIPEMENT.csv",row.names=FALSE)# export en csv
rm(tab_freq)
```

```{r exemple, eval = FALSE}



#Cr?ation tableau de contingence pour la variable RADIO
VIVIENDA_49 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$RADIO, weights = VIVIENDA15$FACTOR ))
Code_Mun <- VIVIENDA_49[,1]
RADIO_SI <- VIVIENDA_49[1:125,3]
RADIO_NO <- VIVIENDA_49[126:250,3]
RADIO_9<- VIVIENDA_49[251:375,3]

TabRadio<- data.frame(Code_Mun,RADIO_SI,RADIO_NO,RADIO_9)

#Cr?ation tableau de contingence pour la variable TELEVISOR
VIVIENDA_50 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$TELEVISOR, weights = VIVIENDA15$FACTOR ))
Code_Mun <- VIVIENDA_50[,1]
TELEVISOR_SI <- VIVIENDA_50[1:125,3]
TELEVISOR_NO <- VIVIENDA_50[126:250,3]
TELEVISOR_9<- VIVIENDA_50[251:375,3]

TabTelevisor<- data.frame(Code_Mun,TELEVISOR_SI,TELEVISOR_NO,TELEVISOR_9)

#Cr?ation tableau de contingence pour la variable TELEVISOR_PP
VIVIENDA_51 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$TELEVISOR_PP, weights = VIVIENDA15$FACTOR ))
Code_Mun <- VIVIENDA_51[,1]
TELEVISOR_PP_SI <- VIVIENDA_51[1:125,3]
TELEVISOR_PP_NO <- VIVIENDA_51[126:250,3]
TELEVISOR_PP_9<- VIVIENDA_51[251:375,3]

TabTelevisorPP<- data.frame(Code_Mun,TELEVISOR_PP_SI,TELEVISOR_PP_NO,TELEVISOR_PP_9)

#Cr?ation tableau de contingence pour la variable COMPUTADORA
VIVIENDA_52 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$COMPUTADORA, weights = VIVIENDA15$FACTOR ))
Code_Mun <- VIVIENDA_52[,1]
COMPUTADORA_SI <- VIVIENDA_52[1:125,3]
COMPUTADORA_NO <- VIVIENDA_52[126:250,3]
COMPUTADORA_9<- VIVIENDA_52[251:375,3]

TabComputadora<- data.frame(Code_Mun,COMPUTADORA_SI,COMPUTADORA_NO,COMPUTADORA_9)

#Cr?ation tableau de contingence pour la variable TELEFONO
VIVIENDA_53 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$TELEFONO, weights = VIVIENDA15$FACTOR ))
Code_Mun <- VIVIENDA_53[,1]
TELEFONO_SI <- VIVIENDA_53[1:125,3]
TELEFONO_NO <- VIVIENDA_53[126:250,3]
TELEFONO_9<- VIVIENDA_53[251:375,3]

TabTabTelephono<- data.frame(Code_Mun,TELEFONO_SI,TELEFONO_NO,TELEFONO_9)

#Cr?ation tableau de contingence pour la variable CELULAR
VIVIENDA_54 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$CELULAR, weights = VIVIENDA15$FACTOR ))
Code_Mun <- VIVIENDA_54[,1]
CELULAR_SI <- VIVIENDA_54[1:125,3]
CELULAR_NO <- VIVIENDA_54[126:250,3]
CELULAR_9<- VIVIENDA_54[251:375,3]

TabCelular<- data.frame(Code_Mun,CELULAR_SI,CELULAR_NO,CELULAR_9)

#Cr?ation tableau de contingence pour la variable INTERNET
VIVIENDA_55 <- as.data.frame(wtd.table(VIVIENDA15$code_mun,VIVIENDA15$INTERNET, weights = VIVIENDA15$FACTOR ))
Code_Mun <- VIVIENDA_55[,1]
INTERNET_SI <- VIVIENDA_55[1:125,3]
INTERNET_NO <- VIVIENDA_55[126:250,3]
INTERNET_9<- VIVIENDA_55[251:375,3]

TabInternet<- data.frame(Code_Mun,INTERNET_SI,INTERNET_NO,INTERNET_9)

# Fusion de l'ensemble des tables correspondant ? chaque type d'?quipement
TabTransition <-cbind(TabRefregirador,TabLavadora,TabHorno,TabAutodrop,TabRadio,TabTelevisor,TabTelevisorPP,TabComputadora,TabTabTelephono,TabCelular,TabInternet)
TabTransition2 <- TabTransition[ , - c(5,9,13,17,21,25,29,33,37,41)] #suppression des doublons colonnes
TabEquipement <- unique(TabTransition2) #suppression des doublons lignes
```

