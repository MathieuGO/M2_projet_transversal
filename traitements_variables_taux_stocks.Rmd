---
title: "variables_taux_stocks"
author: "Nicolas"
date: "28 novembre 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


Dépendances de cartography:

  284  sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
  285  sudo apt-cache policy libgdal-dev
  286  sudo apt update
  287  sudo apt-cache policy libgdal-dev
  290  sudo apt install libgdal-dev



```{r}
# Nettoyage l'espace de travail
rm(list=ls())
getwd()
library('tidyverse')
library("cartography")
```

```{r}
X2015_VIVIENDA_EQUIPEMENT <- read_csv("docs/data/traitees/2015_VIVIENDA_EQUIPEMENT.csv", 
    col_types = cols(Code_Mun = col_character()))
# View(X2015_VIVIENDA_EQUIPEMENT)
```

# Cartographie exploratoire




